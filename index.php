<?php
date_default_timezone_set('America/Mexico_City');
error_reporting(E_ALL); // Error/Exception engine, always use E_ALL
ini_set('display_errors', 1);
ini_set('upload_max_size', '256M');
ini_set('post_max_size', '256M');
ini_set('max_execution_time', '600');

header("Content-type: application/json; charset=utf-8");
$response =  new stdClass();
require('./classes/functions.php');
set_error_handler("myErrorHandler");

require('./classes/parseXml.php');
$xmlschema = 'GLogXML.xsd';
$storageIn = './xmlstorage/';
$response->code = 500;
$response->message = "Acción inválida";

$fp = fopen('data.txt', 'w');

$pet = getenv('REQUEST_METHOD');
$xmlString = file_get_contents('php://input');
fwrite($fp, "APACHE:\n");
$headers = apache_request_headers();
foreach ($headers as $header => $value) {
	fwrite($fp, "$header: $value\n");
}

fwrite($fp, "\n");
fwrite($fp, "ARCHIVO:\n");
fclose($fp);

$parser = new parseXml();

$server = $_SERVER['SERVER_NAME'];
if($server == 'localhost'){
	$UserName = $headers['UserName'];
	$Password = $headers['Password'];
}
else if($server == '201.131.96.135'){
	$UserName = $headers['USERNAME'];
	$Password = $headers['PASSWORD'];
}

$User = $parser->validateUser($UserName);

if($User!=false){
	if(sha1($Password)==$User->Pass){
		if($User->Estatus==1){
			if(trim($xmlString)!=''){
				$parser->test($xmlString);
				$exists = $parser->validExist($xmlString);
				if($exists==0){
					$parser->parseFile($xmlString);
					$response->code = 200;
					$response->message = "El archivo es válido y se subió con éxito.";
				}
				elseif($exists>0){
					$response->code = 403;
					$response->message = "El archivo ya fue procesado previamente.";
				}
			}
			else{
				$response->code = 500;
				$response->message = "No se envió ningun archivo";
			}
		}
		else{
			$response->code = 403;
			$response->message = "Usuario inactivo";
		}
	}
	else{
		$response->code = 403;
		$response->message = "La contraseña es incorrecta";
	}
}
else{
	$response->code = 403;
	$response->message = "El usuario no existe.";
}

echo json_encode($response, JSON_PRETTY_PRINT);
?>
