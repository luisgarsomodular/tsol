<?php
date_default_timezone_set('America/Mexico_City');
error_reporting(E_ALL); // Error/Exception engine, always use E_ALL
ini_set('display_errors', 1);
// header("Content-type: application/xml; charset=utf-8");
header("Content-type: text/plain; charset=utf-8");
require_once('classes/Mysqlconn.php');
require('classes/functions.php');
require('classes/getInfo.php');
require('classes/Shipments.php');

$info = new Shipments();

$activeShipments = $info->getActiveShipments();


$i=0;
foreach ($activeShipments as $activeShipment) {
	$nextStop = $info->getNextStop($activeShipment['TransmissionId']);
	$tractoPosition = $info->activePosition($activeShipment['Tracto']);
	$remolquePosition = $info->activePosition($activeShipment['Remolque']);
	$statusTracto = $info->doShipmentStatus($activeShipment, $nextStop, $tractoPosition);
	$statusRemolque = $info->doShipmentStatus($activeShipment, $nextStop, $remolquePosition);
	$i++;
}

$endpoint = 'https://otmgtm-test-a617854.otm.us2.oraclecloud.com/GC3/glog.integration.servlet.WMServlet'; //TESTING
// $endpoint = 'https://otmgtm-a617854.otm.us2.oraclecloud.com/GC3/glog.integration.servlet.WMServlet'; //PRODUCTION
$Auth['SenderID'] = 'UDA_OTM';
$Auth['Username'] = 'UDA';
$Auth['Password'] = 'L4nds74r2021';
// $Auth['SenderID'] = 'UDA_OTM';



$ShipmentsData = $info->getInbound();
foreach($ShipmentsData as $ShipmentData){
	$ShipmentStatus = notNullValues($ShipmentData);
	$xml = doXml($ShipmentStatus, $Auth);
	$curl = curl_init();
	curl_setopt_array($curl, [
		CURLOPT_PORT	=>443,
		CURLOPT_URL 	=> $endpoint,
		CURLOPT_POSTFIELDS => $xml,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_HEADER => 0,
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_HTTPHEADER => array(
    	"Username:".$Auth['Username'],
    	"Password:".$Auth['Password']
    	)
		]);
	echo $data = curl_exec($curl);
	// $responseCode   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	// $downloadLength = curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
	// $info->updateProcessed($ShipmentStatus['StatusId']);
	// $info->saveResponse($data);
}

/*

if(curl_errno($curl))
{
  print curl_error($curl);
}
else
{
  if($responseCode == "200") echo "successful request";
  echo " # download length : " . $downloadLength;
  curl_close($curl);
  // fclose($fileHandle);
}

var_dump($data);
*/
?>