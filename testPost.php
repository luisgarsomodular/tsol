<?php
date_default_timezone_set('America/Mexico_City');
error_reporting(E_ALL); // Error/Exception engine, always use E_ALL
ini_set('display_errors', 1);
ini_set('upload_max_size', '256M');
ini_set('post_max_size', '256M');
ini_set('max_execution_time', '600');
require('./classes/functions.php');
require('./classes/parseTest.php');
set_error_handler("myErrorHandler");

$fp = fopen('data.txt', 'w');
$pet = getenv('REQUEST_METHOD');
$xmlString = file_get_contents('php://input');
fwrite($fp, "APACHE:\n");
$headers = apache_request_headers();
foreach ($headers as $header => $value) {
	fwrite($fp, "$header: $value\n");
}

fwrite($fp, "\n");
fwrite($fp, "ARCHIVO:\n");
fclose($fp);

$parser = new parseTest();
$parser->test($xmlString);
?>
