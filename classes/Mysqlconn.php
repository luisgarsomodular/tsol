<?php
class Mysqlconn
{

	private $host;
	private $usuario;
	private $pass;
	private $db;

	private $connection;

	function __construct($host, $usuario, $pass, $db)
	{
    $this->host = $host;
    $this->usuario = $usuario;
    $this->pass = $pass;
    $this->db = $db;
	}

	//Create Database connecion
  function connect(){
		$this->connection = mysqli_connect(
			$this->host,
			$this->usuario,
			$this->pass,
			$this->db
		);
    // $this->connection->set_charset("utf8");
    if (mysqli_connect_errno()) {
      print("error al conectarse");
    }
  }

  //Prepare Data array for SQL statement
  function prepareInsert($data){
  	$keys = [];
  	$vals = [];
  	foreach ($data as $key => $value) {
  		array_push($keys, $key);
  		if($value=='' || $value==NULL){
  			array_push($vals, '');

  		}else{
  			array_push($vals, strip_tags($value));

  		}
  	}
		$fields = "`". implode("`, `", $keys) ."`";
		$values = "'". implode("', '", $vals) ."'";
	  return array($fields, $values);
  }

  function prepareUpdate($data){
    $array = [];
    foreach ($data as $key => $value) {
      $array[] = "`$key`='$value'";
    }
  $data = implode(', ', $array);
  return $data;
  }


function prepareCondition($conditionsArray){
  $conditions = [];
  foreach($conditionsArray as $key => $value){
    $conditions[] = "$key='$value'";
  }
  $filter = implode(' AND ', $conditions);
return $filter;
}

 	function insert($table, $data){
 		list($fields, $values) = $this->prepareInsert($data);
    $sql = "INSERT INTO ".$table." (".$fields.") VALUES (".$values.")";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      return $this->getLastId();
    }
    else {
      throw new Exception($error);
    }
 	}


  function  update($table, $data, $condition){
    $data = $this->prepareUpdate($data);
    $condition = $this->prepareCondition($condition);
    $sql = "UPDATE $table SET $data WHERE $condition";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      return $result;
    }
    else {
      throw new Exception($error);
      return $false;
    }

  }


  function insertTest($table, $data){
    $keys = [];
    $vals = [];
    foreach ($data as $key => $value) {
      array_push($keys, $key);
      if($value=='' || $value==NULL){
        array_push($vals, '');
      }else{
        array_push($vals, $value);
      }
    }
    $fields = "`". implode("`, `", $keys) ."`";
    $values = "'". implode("', '", $vals) ."'";

    $sql = "INSERT INTO ".$table." (".$fields.") VALUES (".$values.")";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      return $this->getLastId();
    }
    else {
      throw new Exception($error);
    }
  }


  function insertError($table, $data){
    $keys = [];
    $vals = [];
    foreach ($data as $key => $value) {
      array_push($keys, $key);
      if($value=='' || $value==NULL){
        array_push($vals, '');
      }else{
        array_push($vals, $value);
      }
    }
    $fields = "`". implode("`, `", $keys) ."`";
    $values = "'". implode("', '", $vals) ."'";

    $sql = "INSERT INTO ".$table." (".$fields.") VALUES (".$values.")";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      return $this->getLastId();
    }
    else {
      throw new Exception($error);
    }
  }



	function get($table){
    $data = array();
    $sql = "SELECT * FROM $table";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
          array_push($data, $row);
        }
      }
    }
    else {
      throw new Exception($error);
    }
  return $data;
  }


  function getOne($table){
    $data = FALSE;
    $sql = "SELECT * FROM $table LIMIT 1";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        $data = mysqli_fetch_object($result);
      }
    }
  return $data;
  }




  function getUser($user){
    $data = FALSE;
    $sql = "SELECT * FROM OTM_USERS WHERE Login='".$user."'";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        $data = mysqli_fetch_object($result);
      }
    }
  return $data;
  }



  function getFile($Filename){
    $sql = "SELECT * FROM OTM_FILES WHERE Filename='".$Filename."'";
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    $data = 0;
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        $data = 1;
      }
    }
  return $data;
  }


  function getData($sql){
    $data = array();
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
          array_push($data, $row);
        }
      }
    }
    else {
      throw new Exception($error);
    }
  return $data;
  }

  function getInbound(){
    $sql = "SELECT StatusId, Domain, Xid, ShipmentId, StatusCodeGid, DATE_FORMAT(EventDate, '%Y%m%d%h%i%s') AS EventDate, EventDateTimeZone, EventDateTimeZoneOffset, StatusReasonCodeGid, TimeZoneGID, SSStopSequenceNum, SSStopLocationName, SSStopLocationLatitude, SSStopLocationLongitude, EventGroupGid, StatusGroupGid, ReasonGroupGid, ResponsiblePartyGid, DATE_FORMAT(AttributeDate1GLogDate, '%Y%m%d%h%i%s') AS AttributeDate1, AttributeDate1TZId, AttributeDate1TZOffset, AttributeNumber1, Attribute2, AttributeNumber3, Attribute1, SentDatetime FROM OTM_SHIPMENTSTATUS WHERE SentDatetime IS NULL";
    $data = array();
    $result = mysqli_query($this->connection, $sql);
    $error = mysqli_error($this->connection);
    if (empty($error)) {
      if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
          array_push($data, $row);
        }
      }
    }
    else {
      throw new Exception($error);
    }
  return $data;


  }

    function numRows($sql)
    {
        $result = mysqli_query($this->connection, $sql);
        $error = mysqli_error($this->connection);

        if (empty($error)) {
            return mysqli_num_rows($result);
        } else {
            throw new Exception($error);
        }
    }

    function getDataSingle($sql)
    {
      $result = mysqli_query($this->connection, $sql);
      $error = mysqli_error($this->connection);
      if (empty($error)) {
        if (mysqli_num_rows($result) > 0) {
            return mysqli_fetch_assoc($result);
        }
      } else {
          throw new Exception($error);
      }
      return null;
    }

    function getDataSingleProp($sql, $prop)
    {

        $result = mysqli_query($this->connection, $sql);

        $error = mysqli_error($this->connection);

        if (empty($error)) {
            if (mysqli_num_rows($result) > 0) {
                $data = mysqli_fetch_assoc($result);
                return $data[$prop];
            }
        } else {
            throw new Exception($error);
        }
        return null;
    }

    function executeInstruction($sql)
    {
        $success = mysqli_query($this->connection, $sql);

        $error = mysqli_error($this->connection);

        if (empty($error)) {
            return $success;
        } else {
            throw new Exception($error);
        }
    }



    function getCount($table, $conditionsArray){
      $conditions = [];
      foreach($conditionsArray as $key => $value){
        $conditions[] = "$key='$value'";
      }
      $filter = implode(' AND ', $conditions);
      $sql = "SELECT COUNT(*) AS Total FROM $table WHERE $filter";
      $result = mysqli_query($this->connection, $sql);
      $error = mysqli_error($this->connection);
      if (empty($error)) {
        $data = mysqli_fetch_object($result);
      }
    return $data->Total;
    }



    function activeShipments($date){
      $sql = "SELECT h.*, (SELECT COUNT(*) FROM OTM_SHIPMENTSTOP s WHERE s.`TransmissionId`=h.`TransmissionId`) AS ShipmentStops FROM OTM_SHIPMENTHEADER AS h WHERE '$date' BETWEEN h.`StartGLogDate` AND h.`EndGLogDate`";
      $data = $this->executeResults($sql);
      return $data;
    }


    function currentPosition($tracto){
      $sql = "SELECT  REPLACE(p.`GpsDate`,'T',' ') EventDate, p.* FROM OTM_POSITIONS p WHERE p.`Plate`='$tracto';";
      $data = $this->executeSingleRow($sql);
      return $data;
    }


    function nextPosition($TransmissionId){
      $sql = "SELECT
      l.DomainName,
l.Xid,
l.Latitude,
l.Longitude,
l.LocationName,

s.* FROM OTM_SHIPMENTSTOP s INNER JOIN OTM_LOCATION l ON
  s.TransmissionId=l.TransmissionId AND s.DomainName=l.DomainName AND s.Xid=l.Xid WHERE s.`TransmissionId`='$TransmissionId' AND Arrived =0   ORDER BY StopSequence ASC LIMIT 1;";
      $data = $this->executeSingleRow($sql);
      return $data;
    }

    //Executa la sentencia MySql específica para multiples filas;
    function executeResults($sql){
      $data = array();
      $result = mysqli_query($this->connection, $sql);
      $error = mysqli_error($this->connection);
      if (empty($error)) {
        if (mysqli_num_rows($result) > 0) {
          while ($row = mysqli_fetch_assoc($result)) {
            array_push($data, $row);
          }
        }
      }
      else {
        throw new Exception($error);
      }
    return $data;
    }


    function executeSingleRow($sql){
      $result = mysqli_query($this->connection, $sql);
      $error = mysqli_error($this->connection);
      if (empty($error)) {
        if (mysqli_num_rows($result) > 0) {
          return mysqli_fetch_assoc($result);
        }
        else{
          return null;
        }
      } else {
        throw new Exception($error);
      }
    }




    function close()
    {
        mysqli_close($this->connection);
    }

    function getLastId()
    {
        return mysqli_insert_id($this->connection);
    }
}