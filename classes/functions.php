<?php

if (!function_exists('getallheaders')) {
  function getallheaders() {
  $headers = [];
  foreach ($_SERVER as $name => $value) {
    if (substr($name, 0, 5) == 'HTTP_') {
      $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
    }
  }
  return $headers;
  }
}


if( !function_exists('apache_request_headers') ) {
  function apache_request_headers() {
    $arh = array();
    $rx_http = '/\AHTTP_/';
    foreach($_SERVER as $key => $val) {
      if( preg_match($rx_http, $key) ) {
        $arh_key = preg_replace($rx_http, '', $key);
        $rx_matches = array();
        // do some nasty string manipulations to restore the original letter case
        // this should work in most cases
        $rx_matches = explode('_', $arh_key);
        if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
          foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
          $arh_key = implode('-', $rx_matches);
        }
        $arh[$arh_key] = $val;
      }
    }
    return( $arh );
  }
}


function removeBlank($string){
  $string = trim($string);
  $search = [' ', '-'];
  $replace = ['', ''];
  $string = str_replace($search, $replace, $string);
  return $string;
}


function notNullValues($dataArray){
  $array = [];
  foreach ($dataArray as $key => $value) {
    if($value=='' || $value==NULL){
      $array[$key] = '';
    }
    else{
      $array[$key] = $value;
    }
  }
return $array;
}


function myErrorHandler($errno, $errstr, $errfile, $errline) {
  $ef = fopen('error.log', "a+");
  fwrite($ef, "[$errno] $errstr ----> Error on line $errline in $errfile<br>\n");
  fclose($ef);
}


function doXml($ShipmentStatus, $Auth){

  $xml = '<?xml version="1.0" encoding="UTF-8"?>
  <otm:Transmission xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4" xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4">
    <otm:TransmissionBody>
      <otm:GLogXMLElement>
        <otm:TransactionHeader>
         <otm:SenderSystemID>'.$Auth['SenderID'].'</otm:SenderSystemID>
         <otm:UserName>'.$Auth['Username'].'</otm:UserName>
         <otm:Password>'.$Auth['Password'].'</otm:Password>
         <otm:SenderTransactionId>TrackingEvent</otm:SenderTransactionId>
        </otm:TransactionHeader>
        <otm:ShipmentStatus>
          <otm:ShipmentRefnum>
            <otm:ShipmentRefnumQualifierGid>
              <otm:Gid>
                <otm:Xid>GLOG</otm:Xid>
              </otm:Gid>
            </otm:ShipmentRefnumQualifierGid>
            <otm:ShipmentRefnumValue>'.$ShipmentStatus['ShipmentId'].'</otm:ShipmentRefnumValue>
          </otm:ShipmentRefnum>
          <otm:StatusLevel>SHIPMENT</otm:StatusLevel>
          <otm:StatusCodeGid>
            <otm:Gid>
              <otm:Xid>'.$ShipmentStatus['StatusCodeGid'].'</otm:Xid>
            </otm:Gid>
          </otm:StatusCodeGid>
          <otm:EventDt>
            <otm:GLogDate>'.$ShipmentStatus['EventDate'].'</otm:GLogDate>
            <otm:TZId>'.$ShipmentStatus['EventDateTimeZone'].'</otm:TZId>
            <otm:TZOffset>'.$ShipmentStatus['EventDateTimeZoneOffset'].'</otm:TZOffset>
          </otm:EventDt>
          <otm:StatusReasonCodeGid>
            <otm:Gid>
              <otm:Xid>'.$ShipmentStatus['StatusReasonCodeGid'].'</otm:Xid>
            </otm:Gid>
          </otm:StatusReasonCodeGid>
          <otm:SSStop>';
            if($ShipmentStatus['StatusCodeGid']!='X6' && $ShipmentStatus['StatusCodeGid']!='X7'){
            $xml.='<otm:SSStopSequenceNum>'.$ShipmentStatus['SSStopSequenceNum'].'</otm:SSStopSequenceNum>';
            }

            $xml.='<otm:SSLocation>
              <otm:LocationName>'.$ShipmentStatus['SSStopLocationName'].'</otm:LocationName>
              <otm:Latitude>'.$ShipmentStatus['SSStopLocationLatitude'].'</otm:Latitude>
              <otm:Longitude>'.$ShipmentStatus['SSStopLocationLongitude'].'</otm:Longitude>
            </otm:SSLocation>
          </otm:SSStop>
          <otm:EventGroup>
            <otm:EventGroupGid>
              <otm:Gid>
                <otm:Xid>'.$ShipmentStatus['EventGroupGid'].'</otm:Xid>
              </otm:Gid>
            </otm:EventGroupGid>
          </otm:EventGroup>
          <otm:StatusGroup>
            <otm:StatusGroupGid>
              <otm:Gid>
                <otm:Xid>'.$ShipmentStatus['StatusGroupGid'].'</otm:Xid>
              </otm:Gid>
            </otm:StatusGroupGid>
          </otm:StatusGroup>
          <otm:ReasonGroup>
            <otm:ReasonGroupGid>
              <otm:Gid>
                <otm:Xid>'.$ShipmentStatus['ReasonGroupGid'].'</otm:Xid>
              </otm:Gid>
            </otm:ReasonGroupGid>
          </otm:ReasonGroup>
          <otm:ResponsiblePartyGid>
            <otm:Gid>
              <otm:Xid>'.$ShipmentStatus['ResponsiblePartyGid'].'</otm:Xid>
            </otm:Gid>
          </otm:ResponsiblePartyGid>
          <otm:FlexFieldStringType>
            <otm:Attribute1>'.$ShipmentStatus['Attribute1'].'</otm:Attribute1>
            <otm:Attribute2>'.$ShipmentStatus['Attribute2'].'</otm:Attribute2>
          </otm:FlexFieldStringType>
          <otm:FlexFieldNumbers>
            <otm:AttributeNumber1>'.$ShipmentStatus['AttributeNumber1'].'</otm:AttributeNumber1>
            <otm:AttributeNumber3>'.$ShipmentStatus['AttributeNumber3'].'</otm:AttributeNumber3>
          </otm:FlexFieldNumbers>
          <otm:FlexFieldDates>
            <otm:AttributeDate1>
              <otm:GLogDate>'.$ShipmentStatus['AttributeDate1'].'</otm:GLogDate>
              <otm:TZOffset>'.$ShipmentStatus['ResponsiblePartyGid'].'</otm:TZOffset>
            </otm:AttributeDate1>
          </otm:FlexFieldDates>
          <otm:ShipmentGid>
            <otm:Gid>
              <otm:DomainName>'.$ShipmentStatus['Domain'].'</otm:DomainName>
              <otm:Xid>'.$ShipmentStatus['Xid'].'</otm:Xid>
            </otm:Gid>
          </otm:ShipmentGid>
        </otm:ShipmentStatus>
      </otm:GLogXMLElement>
    </otm:TransmissionBody>
  </otm:Transmission>';

/*
$xml = '<?xml version="1.0" encoding="UTF-8"?><otm:Transmission xmlns:otm="http://xmlns.oracle.com/apps/otm/transmission/v6.4" xmlns:gtm="http://xmlns.oracle.com/apps/gtm/transmission/v6.4"><otm:TransmissionBody><otm:GLogXMLElement><otm:TransactionHeader><otm:UserName>UDA</otm:UserName><otm:Password>L4nds74r2021</otm:Password><otm:SenderTransactionId>TrackingEvent</otm:SenderTransactionId></otm:TransactionHeader><otm:ShipmentStatus><otm:ShipmentRefnum><otm:ShipmentRefnumQualifierGid><otm:Gid><otm:Xid>GLOG</otm:Xid></otm:Gid></otm:ShipmentRefnumQualifierGid><otm:ShipmentRefnumValue>LME.27517</otm:ShipmentRefnumValue></otm:ShipmentRefnum><otm:StatusLevel>SHIPMENT</otm:StatusLevel><otm:StatusCodeGid><otm:Gid><otm:DomainName>LME</otm:DomainName><otm:Xid>CUSNOGPS</otm:Xid></otm:Gid></otm:StatusCodeGid><otm:TimeZoneGid><otm:Gid><otm:Xid>America/Mexico_City</otm:Xid></otm:Gid></otm:TimeZoneGid><otm:EventDt><otm:GLogDate>20201217165615</otm:GLogDate><otm:TZId>America/Mexico_City</otm:TZId><otm:TZOffset>-05:00</otm:TZOffset></otm:EventDt><otm:StatusReasonCodeGid><otm:Gid><otm:Xid>NS</otm:Xid></otm:Gid></otm:StatusReasonCodeGid><otm:SSStop><otm:SSLocation><otm:LocationName></otm:LocationName><otm:Latitude></otm:Latitude><otm:Longitude></otm:Longitude></otm:SSLocation></otm:SSStop><otm:EventGroup><otm:EventGroupGid><otm:Gid><otm:DomainName>LME</otm:DomainName><otm:Xid>READY_FOR_DELIVERY</otm:Xid></otm:Gid></otm:EventGroupGid></otm:EventGroup><otm:StatusGroup><otm:StatusGroupGid><otm:Gid><otm:DomainName>LME</otm:DomainName><otm:Xid>INCIDENCIAS</otm:Xid></otm:Gid></otm:StatusGroupGid></otm:StatusGroup><otm:ResponsiblePartyGid><otm:Gid><otm:Xid>CARRIER</otm:Xid></otm:Gid></otm:ResponsiblePartyGid><otm:FlexFieldNumbers><otm:AttributeNumber1>2</otm:AttributeNumber1></otm:FlexFieldNumbers><otm:FlexFieldDates><otm:AttributeDate1/></otm:FlexFieldDates><otm:ShipmentGid><otm:Gid><otm:DomainName>LME</otm:DomainName><otm:Xid>27517</otm:Xid></otm:Gid></otm:ShipmentGid></otm:ShipmentStatus></otm:GLogXMLElement></otm:TransmissionBody></otm:Transmission>';
*/

  return $xml;
}


?>

