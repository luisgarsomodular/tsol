<?php
require('./classes/Mysqlconn.php');
class parseXml
{

	public function __construct(){
		if($_SERVER['SERVER_NAME'] == 'localhost'){
			$this->conn = new Mysqlconn('localhost', 'root', '12345678', 'INTEGRACION_OTM');
		}
		else if($_SERVER['SERVER_NAME'] == '201.131.96.135'){
			$this->conn = new Mysqlconn('192.168.6.45', 'sa', '$0lstic3$', 'INTEGRACION_OTM');
		}
		$this->conn->connect();
	}

	function saveError($errorString){
		$errorInfo = array('ErrorMessage'=>$errorString);
		$this->conn->insertError('OTM_ERRORS', $errorInfo);
	}


	function validateUser($User){
		return $data = $this->conn->getUser($User);
	}


	//Check if file has already been parsed
	function validExist($xmlString){
		$conditionsArray = [];
		$reader = new XMLReader();
		$reader->xml($xmlString);
		while($reader->read()) {
			if($reader->nodeType==1 && $reader->name=='otm:SenderTransmissionNo'){
				$value = $reader->readInnerXML();
				$conditionsArray['SenderTransmissionNo'] = $value;
			}
		}
	return $this->conn->getCount('OTM_TRANSMISSIONHEADERS', $conditionsArray);
	}

	function test($xmlString){
		$FileInfo = array('Xml'=>$xmlString);
		$this->conn->insertTest('OTM_TEST', $FileInfo);
	}


	function parseFile($xmlString){
		$reader = new XMLReader();
		$reader->xml($xmlString);
		$blocks = [
			'otm:ShipmentHeader',
			'otm:ShipmentStop',
			'otm:Location'
		];
		while($reader->read()) {
			$element = $reader->name;
			if($reader->nodeType==1){

				if($element == 'otm:TransmissionHeader'){
					$content = $reader->readOuterXML();
					$TransmissionId = $this->parseTransmissionHeader($content);
				}
				elseif(in_array($element, $blocks)){
					$content = $reader->readOuterXML();
					switch($element){
						case 'otm:ShipmentHeader':
							$this->parseHeader($TransmissionId, $content);
							break;
						case 'otm:ShipmentStop':
							$this->parseStop($TransmissionId, $content);
							break;
						case 'otm:Location':
							$this->parseLocation($TransmissionId, $content);
							break;
						default:
							break;
					}
				}
			}
		}
	}


	//Parse OTM document header node
	function parseTransmissionHeader($content){
		$reader = new XMLReader();
		$reader->xml($content);
		$transmissionInfo = [];
		$notifyInfo = [];
		$disable = [ 'otm:TransmissionCreateDt', 'otm:NotifyInfo', 'otm:ContactGid', 'otm:Gid', 'otm:DomainName', 'otm:Xid'];
		$notify = ['otm:ContactGid', 'otm:ExternalSystemGid'];

		$i=0;
		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1 && $reader->name!='otm:TransmissionHeader'){
				if(!in_array($reader->name, $disable) && !in_array($reader->name, $notify) ){
					$key = str_replace('otm:', '', $reader->name);
					$value = trim($reader->readInnerXML());
					$transmissionInfo[$key] = $value;
				}
				elseif(in_array($reader->name, $notify) ){
					$info = $reader->readOuterXML();
					$notifyInfo[$i] = $this->parseNotify($info, $notify);
					$i++;
				}
			}
		}
		$TransmissionId = $this->conn->insert('OTM_TRANSMISSIONHEADERS', $transmissionInfo);
		$notifyInfo['TransmissionId'] = $TransmissionId;
		return $TransmissionId;
	}

	// Parse shipment notifies node
	function parseNotify($info, $notify){
		$data = [];
		$reader = new XMLReader();
		$reader->xml($info);
		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1){
				if(in_array($reader->name, $notify)){
					$data['NotifyType'] = str_replace('otm:', '', $reader->name);
				}
				elseif($reader->name=='otm:Gid'){

				}
				else{
					$data[$reader->name] = trim($reader->readInnerXML());
				}
			}
		}
		return $data;
	}

	// Parse shipment header noder
	function parseHeader($TransmissionId=1, $content){
		if($content!=''){
			$blocks = [
				'otm:ShipmentGid',
				'otm:ServiceProviderGid',
				'otm:StartDt',
				'otm:EndDt',
				'otm:SourceLocationRef',
				'otm:DestinationLocationRef',
				'otm:FlexFieldStrings',
				'otm:FlexFieldNumbers'
				];
			$ShipmentHeader = [];
			$ShipmentHeader['TransmissionId'] = $TransmissionId;
			$reader = new XMLReader();
			$reader->xml($content);
			while($reader->read()) {
				$t = $reader->nodeType;
				if($t==1){
					if(in_array($reader->name, $blocks)){
						$subContent = $reader->readOuterXML();
						$subData = $this->parseContent($subContent);
						// var_dump($subData);
						switch ($reader->name) {
							case 'otm:ShipmentGid':
								if(isset($subData['DomainName'])){ $ShipmentHeader['DomainName'] = $subData['DomainName']; }
								if(isset($subData['Xid'])){ $ShipmentHeader['Xid'] = $subData['Xid']; }
								break;
							case 'otm:ServiceProviderGid':
								if(isset($subData['DomainName'])){ $ShipmentHeader['ServiceProviderDomainName'] = $subData['DomainName']; }
								if(isset($subData['Xid'])){ $ShipmentHeader['ServiceProviderXid'] = $subData['Xid']; }
								break;
							case 'otm:StartDt':
								if(isset($subData['GLogDate'])){ $ShipmentHeader['StartGLogDate'] = $subData['GLogDate']; }
								if(isset($subData['TZId'])){ $ShipmentHeader['StartTZId'] = $subData['TZId']; }
								if(isset($subData['TZOffset'])){ $ShipmentHeader['StartTZOffset'] = $subData['TZOffset']; }
								break;
							case 'otm:EndDt':
								if(isset($subData['GLogDate'])){ $ShipmentHeader['EndGLogDate'] = $subData['GLogDate']; }
								if(isset($subData['TZId'])){ $ShipmentHeader['EndTZId'] = $subData['TZId']; }
								if(isset($subData['TZOffset'])){ $ShipmentHeader['EndTZOffset'] = $subData['TZOffset']; }
								break;
							case 'otm:SourceLocationRef':
								if(isset($subData['DomainName'])){ $ShipmentHeader['SourceDomainName'] = $subData['DomainName']; }
								if(isset($subData['Xid'])){ $ShipmentHeader['SourceXid'] = $subData['Xid']; }
								break;
							case 'otm:DestinationLocationRef':
								if(isset($subData['DomainName'])){ $ShipmentHeader['DestinationDomainName'] = $subData['DomainName']; }
								if(isset($subData['Xid'])){ $ShipmentHeader['DestinationXid'] = $subData['Xid']; }
								break;
							case 'otm:FlexFieldStrings':
								if(isset($subData['Attribute1'])){ $ShipmentHeader['Remolque'] = $subData['Attribute1']; }
								if(isset($subData['Attribute2'])){ $ShipmentHeader['Tracto'] = $subData['Attribute2']; }
								break;
							case 'otm:FlexFieldNumbers':
								if(isset($subData['AttributeNumber1'])){ $ShipmentHeader['AttributeNumber1'] = $subData['AttributeNumber1']; }
								if(isset($subData['AttributeNumber2'])){ $ShipmentHeader['AttributeNumber2'] = $subData['AttributeNumber2']; }
								break;
							default:
								break;
						}
					}
				}
			}
		$this->conn->insert('OTM_SHIPMENTHEADER', $ShipmentHeader);
		}
	}



	// Parse shipment stops nodes
	function parseStop($TransmissionId=1, $content){
		// echo $content;
		$blocks = [
			'otm:StopSequence',
			'otm:LocationRef',
			'otm:LocationRoleGid',
			'otm:ArrivalTime',
			'otm:DepartureTime',
			'otm:StopType'
			];
		$ShipmentStop =  [];
		$ShipmentStop['TransmissionId'] = $TransmissionId;
		$reader = new XMLReader();
		$reader->xml($content);
		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1){
				if(in_array($reader->name, $blocks)){
					$subContent = $reader->readOuterXML();
					$subData = $this->parseContent($subContent);
					switch ($reader->name) {
						case 'otm:StopSequence':
							if(isset($subData['StopSequence'])){ $ShipmentStop['StopSequence'] = $subData['StopSequence']; }
							break;
						case 'otm:LocationRef':
							if(isset($subData['DomainName'])){ $ShipmentStop['DomainName'] = $subData['DomainName']; }
							if(isset($subData['Xid'])){ $ShipmentStop['Xid'] = $subData['Xid']; }
							break;
						case 'otm:LocationRoleGid':
							if(isset($subData['Xid'])){ $ShipmentStop['RoleXid'] = $subData['Xid']; }
							break;
						case 'otm:ArrivalTime':
							if(isset($subData['GLogDate'])){ $ShipmentStop['ArrivalGLogDate'] = $subData['GLogDate']; }
							if(isset($subData['TZId'])){ $ShipmentStop['ArrivalTZId'] = $subData['TZId']; }
							if(isset($subData['TZOffset'])){ $ShipmentStop['ArrivalTZOffset'] = $subData['TZOffset']; }
							break;
						case 'otm:DepartureTime':
							if(isset($subData['GLogDate'])){ $ShipmentStop['DepartureGLogDate'] = $subData['GLogDate']; }
							if(isset($subData['TZId'])){ $ShipmentStop['DepartureTZId'] = $subData['TZId']; }
							if(isset($subData['TZOffset'])){ $ShipmentStop['DepartureTZOffset'] = $subData['TZOffset']; }
							break;
						case 'otm:StopType':
							if(isset($subData['StopType'])){ $ShipmentStop['StopType'] = $subData['StopType']; }
							break;
						default:
							break;
					}
				}
			}
		}
	$this->conn->insert('OTM_SHIPMENTSTOP', $ShipmentStop);
	}


	// Parse shipment locations nodes
	function parseLocation($TransmissionId=1, $content){
		$blocks = [
			'otm:LocationGid',
			'otm:LocationName',
			'otm:CountryCode3Gid',
			'otm:TimeZoneGid',
			'otm:Address',
			'otm:AddressLines',
			'otm:LocationRole',
			'otm:IsActive'
			];
		$Location = [];
		$Location['TransmissionId'] = $TransmissionId;
		$reader = new XMLReader();
		$reader->xml($content);


		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1){
				$subContent = $reader->readOuterXML();
				$subData = $this->parseContent($subContent);
				// var_dump($subData);
				switch ($reader->name) {
					case 'otm:LocationGid';
						if(isset($subData['DomainName'])){ $Location['DomainName'] = $subData['DomainName']; }
						if(isset($subData['Xid'])){ $Location['Xid'] = $subData['Xid']; }
						break;
					case 'otm:LocationName':
						if(isset($subData['LocationName'])){ $Location['LocationName'] = $subData['LocationName']; }
						break;
					case 'otm:CountryCode3Gid':
						if(isset($subData['Xid'])){ $Location['CountryXid'] = $subData['Xid']; }
						break;
					case 'otm:TimeZoneGid':
						if(isset($subData['Xid'])){ $Location['TimeZoneXid'] = $subData['Xid']; }
						break;
					case 'otm:Address':
						if(isset($subData['City'])){ $Location['City'] = $subData['City']; }
						if(isset($subData['ProvinceCode'])){ $Location['ProvinceCode'] = $subData['ProvinceCode']; }
						if(isset($subData['PostalCode'])){ $Location['PostalCode'] = $subData['PostalCode']; }
						if(isset($subData['Latitude'])){ $Location['Latitude'] = $subData['Latitude']; }
						if(isset($subData['Longitude'])){ $Location['Longitude'] = $subData['Longitude']; }
						break;
					case 'otm:AddressLines':
						$addressLines = $reader->readOuterXML();
						$Location['Address'] = $this->parseAddress($addressLines);

						break;
					case 'otm:LocationRole':
						if(isset($subData['Xid'])){ $Location['LocationRoleGid'] = $subData['Xid']; }
						break;
					case 'otm:IsActive':
						if(isset($subData['IsActive'])){ $Location['IsActive'] = $subData['IsActive']; }
						break;
					default:
						break;
				}
			}
		}
		$condition = [
			'TransmissionId' => $Location['TransmissionId'],
			'DomainName' => $Location['DomainName'],
			'Xid' => $Location['Xid'],
			'LocationName' => $Location['LocationName']
		];
		$exist = $this->conn->getCount('OTM_LOCATION', $condition);
		if($exist==0){
			$this->conn->insert('OTM_LOCATION', $Location);
		}
	}


	function parseContent($content){
		$reader = new XMLReader();
		$reader->xml($content);
		$data = [];
		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1){
				$key = str_replace('otm:', '', $reader->name);
				$value = trim($reader->readInnerXML());
				$data[$key] = $value;

			}
		 }
	return $data;
	}


	function parseAddress($addressLines){
		$address = '';
		$reader = new XMLReader();
		$reader->xml($addressLines);
		while($reader->read()) {
			$t = $reader->nodeType;
			if($t==1 && $reader->name=='otm:AddressLine'){
				$address.= trim( $reader->readInnerXML() ).' ';
			}
		}
		return trim($address);
	}


}




?>