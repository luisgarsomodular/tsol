<?php
class Shipments extends getInfo
{

	function __construct()
	{
		if($_SERVER['SERVER_NAME'] == 'localhost'){
			$this->conn = new Mysqlconn('localhost', 'root', '12345678', 'INTEGRACION_OTM');
		}
		else if($_SERVER['SERVER_NAME'] == '201.131.96.135'){
			$this->conn = new Mysqlconn('192.168.6.45', 'sa', '$0lstic3$', 'INTEGRACION_OTM');
		}
		$this->conn->connect();
	}


	function getActiveShipments(){
    $date = date('Y-m-d H:i:s');
    $activeShipments = $this->conn->activeShipments($date);
    return $activeShipments;
  }

  function activePosition($Unit){
  	$position = $this->conn->currentPosition($Unit);
    return $position;
  }

  function getNextStop($TransmissionId){
  	$stop = $this->conn->nextPosition($TransmissionId);
    return $stop;
  }



  function doShipmentStatus($activeShipment, $nextStop, $unitPosition){
		$ShipmentStatus = [];
		$ShipmentStatus['ShipmentId'] = $activeShipment['DomainName'].'.'.$activeShipment['Xid'];
		$ShipmentStatus['Domain'] = $activeShipment['DomainName'];
		$ShipmentStatus['Xid'] = $activeShipment['Xid'];
		$stopId = $nextStop['StopId'];
		if($unitPosition!=NULL){
			$travelData =  $this->fnInfoTravelMap($unitPosition['Latitude'], $unitPosition['Longitude'], $nextStop['Latitude'], $nextStop['Longitude']);
			$distance = round($travelData['distanceTravel'], 1);
			$eta = gmdate("H:i:s", $travelData['timeTravel']);
 			$ShipmentStatus['EventDate'] = $unitPosition['EventDate'];
			$ShipmentStatus['EventDateTimeZone'] = $this->fnZonaHoraria();
			$ShipmentStatus['EventDateTimeZoneOffset'] = $unitPosition['Zone'];
 			$ShipmentStatus['TimeZoneGID'] = $unitPosition['EventDate'];
			$ShipmentStatus['StatusReasonCodeGid'] = 'NS';
			$ShipmentStatus['SSStopLocationName'] = $nextStop['LocationName'];
      $ShipmentStatus['SSStopLocationLatitude'] = $nextStop['Latitude'];
      $ShipmentStatus['SSStopLocationLongitude'] = $nextStop['Longitude'];
      $ShipmentStatus['ResponsiblePartyGid'] = 'CARRIER';
      $ShipmentStatus['ReasonGroupGid'] = 'NO_ERROR';
			$ShipmentStatus['SSStopSequenceNum'] = $nextStop['StopSequence'];
			$ShipmentStatus['AttributeDate1GLogDate'] = $unitPosition['EventDate'];
			$ShipmentStatus['AttributeDate1TZId'] = $this->fnZonaHoraria();
			$ShipmentStatus['AttributeDate1TZOffset'] = $this->fnZonaHoraria();
			$ShipmentStatus['Attribute2'] = $distance;
/*
      $ShipmentStatus['Attribute1'] //verificar si se tiene opción de abandono de ruta
      $ShipmentStatus['AttributeNumber3'] // Validar si se tiene opción de verificar posición en un mismo lugar por mas de 12 minutos
*/
			if($distance<1.5){
				//Actualizar a arrivo
				//Enviar evento de arribo
				$ShipmentStatus['StatusGroupGid'] = 'ARRIVED';
				if($nextStop['StopSequence']<$activeShipment['ShipmentStops']){
	        $ShipmentStatus['EventGroupGid'] = 'ARS';
				}
				elseif($nextStop['StopSequence']==$activeShipment['ShipmentStops']){
	        $ShipmentStatus['EventGroupGid'] = 'X4';
				}
				$updArrive = $this->conn->update('OTM_SHIPMENTSTOP', array('Arrived'=>1), array('StopId'=>$stopId));
			}
			else{
				$ShipmentStatus['SSStopSequenceNum'] = NULL;
				$ShipmentStatus['AttributeNumber1'] = $nextStop['StopSequence'];

				if($nextStop['StopSequence']<$activeShipment['ShipmentStops']){
				$ShipmentStatus['StatusCodeGid'] = 'X6';
				$ShipmentStatus['StatusGroupGid'] = 'DEPARTED';
				}
				elseif($nextStop['StopSequence']==$activeShipment['ShipmentStops']){
				$ShipmentStatus['StatusCodeGid'] = 'X7';
        $ShipmentStatus['EventGroupGid'] = 'ENROUTE LOCATION UPDATE ACTUAL';
				}


			}


	$this->conn->insert('OTM_SHIPMENTSTATUS', $ShipmentStatus);


	}
	else{ //No data from GPS


	}

  }


	function getInbound(){
		return $data = $this->conn->getInbound();
	}


	function saveResponse($data){
		echo $data;
		$this->conn->insert('OTM_INBOUNDRESPONSE', array('Response'=>$data));
	}


	function updateProcessed($StatusId){
		$this->conn->update('OTM_SHIPMENTSTATUS', array('SentDatetime'=>date('Y-m-d H:i:s')), array('StatusId'=>$StatusId));
	}



}//end of class
 ?>