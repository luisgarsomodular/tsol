<?php
class getInfo
{

  function getActive(){
    $activeShipments = $this->conn->Active();

  }


	function fnInfoTravelMap($iOrLatitud,$iOrLongitud,$iDestLatitud,$iDestLongitud,$iTipo='-1',$iAltura='-1',$iLongitud='-1',$iAncho='-1',$iPeso='-1',$iTotalCajas='-1'){

		$aConfigMaps = array(
			'key'   => 'aV4jkx1snSCk2pdqSqjl',
      'secret'=> '1dmNSunSdAKpue3Ccqkx1g',
      'google'=> 'AIzaSyBTBZIN5X9lsstyry5yGDwC4s_Z3IXkIC0'
    );

    $iStatus     = false;
    $iTime       = $iDistance = 0;
    $sComplement = '';

    if($iTipo!='-1' && $iTipo!=""){ $sComplement .= '&truckType='.$iTipo; }
	  if($iAltura!='-1' && $iAltura!=""){ $sComplement .= '&height='.$iAltura; }
    if($iLongitud!='-1' && $iLongitud!=""){ $sComplement .= '&length='.$iLongitud; }
    if($iAncho!='-1' && $iAncho!=""){ $sComplement .= '&width='.$iAncho; }
		if($iPeso!='-1' && $iPeso!=""){ $sComplement .= '&limitedWeight='.$iPeso; }
		if($iTotalCajas!='-1' && $iTotalCajas!=""){ $sComplement .= '&trailersCount='.$iTotalCajas; }

    $map_key = 'aV4jkx1snSCk2pdqSqjl';
    $app_key = '1dmNSunSdAKpue3Ccqkx1g';

    try{
      //https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0=22.008523%2C-100.837506&waypoint1=22.003720%2C-100.830740&mode=fastest%3Btruck%3Btraffic%3Aenabled&departure=now&app_id=devportal-demo-20180625&app_code=9v2BkviRwi9Ot26kp2IysQ

      $sUrl      = 'https://route.cit.api.here.com/routing/7.2/calculateroute.json?&waypoint0='.$iOrLatitud.','.$iOrLongitud.'&waypoint1='.$iDestLatitud.','.$iDestLongitud.'&mode=fastest;car;traffic:disabled&departure&app_id='.$map_key.'&app_code='.$app_key;

      $sUrl;
      $sgetReq   = curl_init();
      $options = array(
        CURLOPT_PORT  =>443,
        CURLOPT_URL            => $sUrl,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false
      );

      curl_setopt_array( $sgetReq, $options );
      $json = curl_exec($sgetReq);
      $responseCode   = curl_getinfo($sgetReq, CURLINFO_HTTP_CODE);
      $objResult = json_decode($json, true);
      if($objResult){
        if(isset($objResult['response']['route'])){
          $iStatus   = 1;
          $iTime     = $objResult['response']['route'][0]['summary']['travelTime'];   //Tiempo en Segundos
          $iDistance = $objResult['response']['route'][0]['summary']['distance'];
        }
        else{
          $iStatus   = 1;
          //print_r($objResult);
          $sMsgError = $objResult['type']."\n".$objResult['details'];
          //fnRegisterLog("".__FUNCTION__,implode("\n",func_get_args()),'Error Here Maps '.$sMsgError);
          //$aResultado = fnGetRouteMap($iOrLatitud,$iOrLongitud,$iDestLatitud,$iDestLongitud);
          $iTime     = 0;     //Tiempo en Segundos
          $iDistance = 0;
        }
    	}
    else{
        //fnRegisterLog("".__FUNCTION__,implode("\n",func_get_args()),'Error en llamado API');
    }
  }
  catch (Exception $e){
    echo 'Error wbs 8.1 maps ';
  }

	$aResult = array(
		'status' => $iStatus,
		'timeTravel' => ($iTime),
		'distanceTravel' => ($iDistance/1000),
		'url'            => $sUrl);
	return $aResult;
	}


	function distancia_puntos($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
		// Cálculo de la distancia en grados
		$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
		// Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
		switch($unit) {
			case 'km':
			$distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
			break;
			case 'mi':
			$distance = $degrees * 69.05482; // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
			break;
			case 'nmi':
			$distance =  $degrees * 59.97662; // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)
		}
		return round($distance, $decimals);
	}

function fnZonaHoraria(){
  $res = '-00:00';
  if($_SERVER['SERVER_NAME'] == 'localhost'){
    $link = mysqli_connect("localhost", "root", "12345678", "INTEGRACION_OTM");
  }
  else if($_SERVER['SERVER_NAME'] == '201.131.96.135'){
    $link = mysqli_connect("192.168.6.45", "sa", "$0lstic3$", "ALG_BD_CORPORATE_SAVL");
  }
  if ($link){
    $sql = "SELECT TIME_FORMAT(TIMEDIFF(CURRENT_TIMESTAMP,UTC_TIMESTAMP()), '%H:%i')  AS HORARIO;";
    if ($qry = mysqli_query($link,$sql)){
      $row = mysqli_fetch_object($qry);
      $res = $row->HORARIO;
      $res = str_replace('-','+', $res);
    }
    mysqli_free_result($qry);
    mysqli_close($link);
  }
  return $res;
}

function fnDameHoraUTC($fecha,$zona){
  $res = '-00:00';
  if($_SERVER['SERVER_NAME'] == 'localhost'){
    $link = mysqli_connect("localhost", "root", "12345678", "INTEGRACION_OTM");
  }
  else if($_SERVER['SERVER_NAME'] == '201.131.96.135'){
    $link = mysqli_connect("192.168.6.45", "sa", "$0lstic3$", "ALG_BD_CORPORATE_SAVL");
  }
  if ($link){
    $sql = "SELECT CONVERT_TZ('".$fecha."','+00:00','".$zona."') as FECHA";
    if ($qry = mysqli_query($link,$sql)){
      $row = mysqli_fetch_object($qry);
      $res = $row->FECHA;
      $res = str_replace(' ','T', $res);
    }
    mysqli_free_result($qry);
    mysqli_close($link);
  }
  return $res;
}

function showMethods(){
  $soap_client = new SoapClient('http://ws.grupouda.com.mx/wsUDAHistoryGetByPlate.asmx?wsdl');
	return $result = $soap_client->__getFunctions();
}

//http://ws.grupouda.com.mx/wsUDAHistoryGetByPlate.asmx?op=HistoyDataLastLocationByUser
function getPositions($user,$pass){
  $data = [];
  $client = new SoapClient('http://ws.grupouda.com.mx/wsUDAHistoryGetByPlate.asmx?wsdl');
  $param = array(
    'sLogin' => $user,
    'sPassword' => $pass
  );
  $result = $client->HistoyDataLastLocationByUser($param);
  if (is_object($result)){
    $x = get_object_vars($result);
    $y = get_object_vars($x['HistoyDataLastLocationByUserResult']);
    $xml = $y['any'];
    if($xml2 = simplexml_load_string($xml)){
      $info = $xml2->Response;
      $total = count($info->Plate);
      for($i = 0 ; $i < $total ; $i++){
        $imei    = (string) $info->Plate[$i]['id'];
        $data[$i]['Imei']    = substr($imei, -7);
        $gpsdate = (string) $info->Plate[$i]->hst->DateTimeGPS;
        $data[$i]['Latitude'] = (string)$info->Plate[$i]->hst->Latitude;
        $data[$i]['Longitude'] = (string)$info->Plate[$i]->hst->Longitude;
        $data[$i]['Speed'] = (string)$info->Plate[$i]->hst->Speed;
        $data[$i]['Angle'] = (string)$info->Plate[$i]->hst->Heading;
        $event = (string)$info->Plate[$i]->hst->Event;
        $data[$i]['Event'] = substr($event,0,50);
        $data[$i]['Plate'] = (string) removeBlank($info->Plate[$i]->hst->Plate);
        $data[$i]['Eco'] = (string) removeBlank($info->Plate[$i]->hst->ECO);
        $data[$i]['Imei'] = (string)$info->Plate[$i]->hst->Imei;
        $data[$i]['EventId'] = (string)$info->Plate[$i]->hst->EventID;
        $data[$i]['Location'] = (string)$info->Plate[$i]->hst->Location;
        try {
          $zone = $this->fnZonaHoraria();
          $gpsdate = $this->fnDameHoraUTC($gpsdate,$zone);
          $data[$i]['Zone'] = $zone;
          $data[$i]['GpsDate'] = str_replace("/", "-", $gpsdate);
        } catch (Exception $e) {
            print_r($e->getMessage()."\n");
        }
      }
    }
  }
  return $data;
}




function fnObtienePosiciones($usuario,$password,$token,$cliente){
  $soap_client = new SoapClient('http://ws.grupouda.com.mx/wsUDAHistoryGetByPlate.asmx?wsdl');
  /*arma arreglo con variables
  Falta una funcion que calcule la fecha con menos díez minutos y la mande en el formato
  YYYY-mm-ddThh:mm:ss la T es importante por que asi la toma como fecha ISO
  */
  $param = array('sLogin' => $usuario,
                 'sPassword' => $password);
//  print_r($param);
  /*toma el resultado*/
  $result=$soap_client->HistoyDataLastLocationByUser($param);
  //print_r($result);
  //obtiene el primer objeto
  if (is_object($result)){
    //echo "si es";
    $x = get_object_vars($result);
    //print_r($x);
    //print_r($x['HistoyDataLastLocationByUserResult']);
    //obtiene el objeto contenido en la petición
    $y = get_object_vars($x['HistoyDataLastLocationByUserResult']);
    //print_r($y['any']);
    //obtiene el xml de la petición
    $xml = $y['any'];
    // if strpos($xml,'<description>OK'){
    if($xml2 = simplexml_load_string($xml)){
      //$obj_unidades = $xml2->Plate;
      //$z = get_object_vars($x['Response']);
      $cuenta = count($xml2->Response->Plate);
      //echo "elementos ".$cuenta;
      $c = 0;
      echo $cliente;
      for($i = 0 ; $i < count($xml2->Response->Plate) ; $i++){
        $imei    = (string) $xml2->Response->Plate[$i]['id'];
        $imei    = substr($imei, -7);
        $gpsdate = (string) $xml2->Response->Plate[$i]->hst->DateTimeGPS;


        $lat = (string)$xml2->Response->Plate[$i]->hst->Latitude;
        $lon = (string)$xml2->Response->Plate[$i]->hst->Longitude;
        $vel = (string)$xml2->Response->Plate[$i]->hst->Speed;
        $angulo = (string)$xml2->Response->Plate[$i]->hst->Heading;
        $evento = (string)$xml2->Response->Plate[$i]->hst->Event;
        $evento = substr($evento,0,50);
        $placa = (string)$xml2->Response->Plate[$i]->hst->Plate;
        $eco = (string)$xml2->Response->Plate[$i]->hst->ECO;
        $imei = (string)$xml2->Response->Plate[$i]->hst->Imei;
        $id_event = (string)$xml2->Response->Plate[$i]->hst->EventID;
        $dir = (string)$xml2->Response->Plate[$i]->hst->Location;
        try {
          echo $placa.",\n";
          $zona = fnZonaHoraria();
          $gpsdate = fnDameHoraUTC($gpsdate,$zona);
          $gpsdate = str_replace("/", "-", $gpsdate);
          //echo $gpsdate.",\n";
          //AQUI VA EL RESTO DE TUS FUNCIONES
        } catch (Exception $e) {
            print_r($e->getMessage()."\n");
        }
       $c = $c+1;
      }
    } else {
        echo 'Not XML';
    }
    //(())} else {
      //fallo la peticion
    //}
  } else {
    echo "no es";
  }
  //echo " total enviados: ".$c;
}



}


 ?>